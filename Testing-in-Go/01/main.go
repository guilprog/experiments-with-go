package main

import "fmt"

func main() {
	fmt.Println(sum(4, 3))
}
func sum(xi ...int) int {
	var val int
	for _, v := range xi {
		val += v

	}
	return val + 1
}
