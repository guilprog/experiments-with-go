package main

import "testing"

func TestSum(t *testing.T) {
	value := sum(3, 4)
	if value != 7 {
		t.Error("Expected:", 7, "Got", value)
	}
}
