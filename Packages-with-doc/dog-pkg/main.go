package main

import (
	"fmt"

	"gitlab.com/guille/experiments-with-go/Packages-with-doc/dog-pkg/dog"
)

type can struct {
	name string
	age  int
}

func main() {
	c := can{
		name: "Balton",
		age:  dog.Age(11),
	}
	fmt.Printf("Dog name: %v\n Age(human age * 7): %v ", c.name, c.age)
}
