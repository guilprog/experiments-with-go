package main

import (
	"fmt"
	"runtime"
)

func main() {

	fmt.Printf("Operative System: %v \n Architecture: %v", runtime.GOOS, runtime.GOARCH)

}
