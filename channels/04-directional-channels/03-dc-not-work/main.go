package main

import "fmt"

func main() {
	//buffered channel
	//in this case the number of values to ​​sent through the channel is 1 in the buffer
	ca := make(chan<- int, 2)

	ca <- 42
	ca <- 43

	fmt.Println(<-ca)
	fmt.Println(<-ca)
	fmt.Println("---------------")
	fmt.Printf("%T\n", ca)
}
