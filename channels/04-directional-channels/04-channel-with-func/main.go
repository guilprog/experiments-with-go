package main

import "fmt"

func main() {
	c := make(chan int)

	//send
	go sendIt(c)

	//receive
	receiveIt(c)

	fmt.Println("Finalizando")
}

func sendIt(c chan<- int) {
	c <- 42
}
func receiveIt(c <-chan int) {
	fmt.Println(<-c)
}
