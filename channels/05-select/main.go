package main

import "fmt"

func main() {
	pair := make(chan int)
	odd := make(chan int)
	exit := make(chan int)

	go send(pair, odd, exit)

	receive(pair, odd, exit)
	fmt.Println("Finalizando")

}

func send(p, o, e chan<- int) {
	for j := 1; j < 100; j++ {
		if j%2 == 0 {
			p <- j
		} else {
			o <- j
		}

	}

	e <- 0
}

func receive(p, o, e <-chan int) {
	for {
		select {
		case v := <-p:
			fmt.Println("Pairs: \n", v)
		case v := <-o:
			fmt.Println("Odds: \n", v)
		case v := <-e:
			fmt.Println("Exit:", v)
			return
		}

	}

}
