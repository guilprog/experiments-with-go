package main

import (
	"fmt"

	"gitlab.com/guille/experiments-with-go/003-packages/cat"
)

func main() {
	fmt.Println("Hello from DOG")
	cat.Hola()
	cat.Comen()

}
