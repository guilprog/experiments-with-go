package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("CPUs:", runtime.NumCPU())
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	var wg sync.WaitGroup

	increase := 0

	grs := 100
	wg.Add(grs)
	for i := 0; i < grs; i++ {
		go func() {
			v := increase
			runtime.Gosched()
			v++
			increase = v

			fmt.Println(increase)
			wg.Done()

		}()

	}
	wg.Wait()
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	fmt.Println("Increase final value is: ", increase)
}
