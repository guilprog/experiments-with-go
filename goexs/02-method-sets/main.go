package main

import "fmt"

type person struct {
	name string
}
type human interface {
	speak() string
}

func (p *person) speak() string {
	return p.name
}
func saySomething(h human) {
	fmt.Println("Hi, I'm ", h.speak())
}
func main() {
	p := person{
		"Guille",
	}
	saySomething(&p)
}
