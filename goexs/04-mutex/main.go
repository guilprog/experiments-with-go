package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Println("CPUs:", runtime.NumCPU())
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	var wg sync.WaitGroup
	var m sync.Mutex
	increase := 0

	grs := 100
	wg.Add(grs)
	for i := 0; i < grs; i++ {
		go func() {
			m.Lock()
			v := increase
			v++
			increase = v

			fmt.Println(increase)
			m.Unlock()
			wg.Done()

		}()

	}
	wg.Wait()
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	fmt.Println("Increase final value is: ", increase)
}
