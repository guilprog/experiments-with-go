package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg sync.WaitGroup

func main() {
	fmt.Printf("Número de CPUs al inicio: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al inicio: %v\n", runtime.NumGoroutine())

	wg.Add(2)
	go firstGo()
	go secGo()
	fmt.Printf("Número de CPUs en el medio: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas en el medio: %v\n", runtime.NumGoroutine())

	wg.Wait()
	fmt.Println("\ngoroutines end")

	fmt.Printf("Número de CPUs al final: %v\n", runtime.NumCPU())
	fmt.Printf("Número de Gorutinas al final: %v\n", runtime.NumGoroutine())

}

func firstGo() {

	for i := 0; i < 20; i++ {

		fmt.Printf("\nFirstGo index %v: ", i)
	}
	wg.Done()
}
func secGo() {

	for i := 0; i < 20; i++ {

		fmt.Printf("\nSecGo index %v:", i)
	}
	wg.Done()
}
