package main

import (
	"fmt"
	"runtime"
	"sync"
	"sync/atomic"
)

func main() {
	fmt.Println("CPUs:", runtime.NumCPU())
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	var wg sync.WaitGroup
	var increase int64

	grs := 100
	wg.Add(grs)
	for i := 0; i < grs; i++ {
		go func() {
			atomic.AddInt64(&increase, 1)
			fmt.Println(atomic.LoadInt64(&increase))
			wg.Done()

		}()

	}
	wg.Wait()
	fmt.Println("Goroutines:", runtime.NumGoroutine())
	fmt.Println("Increase final value is: ", increase)
}
